var gamejs = require('gamejs');
var world = require('./world');

function main() {
   gamejs.display.setCaption("TEst?");
   var game = new world.Game();
   game.startGameScene();
};

gamejs.preload([
    "images/k6.png",
    "images/tortoise.png"
])

gamejs.ready(main);
