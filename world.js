var gamejs = require('gamejs');
var Surface = require('gamejs').Surface;
var mysprites = require('./sprites');

exports.Game = function() {
    var scene = new gamejs.scene.Scene([800, 500]);
    var screenRect = scene.screen.getRect();
   
    this.startGameScene = function () {
        function gameUpdate(msDuration) {
            collidesprites = gamejs.sprite.spriteCollide(you, tortoiseGroup, false);
            if (collidesprites.length > 0) {
                you.heading += 180;
            }
            return;
        };
    
        /**
         * event handling function when game is running
         * @see startGameScene
         */
        function gameDoEvents(event) {
            if (event.type == gamejs.event.KEY_DOWN) {
                if (event.key == gamejs.event.K_RIGHT) {
                    you.speed = you.maxspeed;
                    you.heading = 0;
                } else if (event.key == gamejs.event.K_DOWN) {
                    you.speed = you.maxspeed;
                    you.heading = 90;
                } else if (event.key == gamejs.event.K_LEFT) {
                    you.speed = you.maxspeed;
                    you.heading = 180;
                } else if (event.key == gamejs.event.K_UP) {
                    you.speed = you.maxspeed;
                    you.heading = 270;
                } else if (event.key == gamejs.event.K_SPACE) {
                    
                }
            } else if (event.type == gamejs.event.KEY_UP) {
                if ([gamejs.event.K_LEFT, gamejs.event.K_RIGHT, gamejs.event.K_UP, gamejs.event.K_DOWN].indexOf(event.key) > -1) {
                    you.speed = 0
                } else if (event.key === gamejs.event.K_m) {
                    
                }
            }
        };
    
        scene.stop();
        //scene.background = gamejs.image.load('images/starbackground.png');
  
        // diamonds created by planet explosions
        var tortoiseGroup = new gamejs.sprite.Group();
        scene.addGroup(tortoiseGroup)
        tortoiseGroup.add(new mysprites.Tortoise(scene))
  
        // Player
        var you = new mysprites.You(scene);
        scene.sprites.push(you);
          
        scene.doEvents = gameDoEvents;
        scene.update = gameUpdate;
        scene.start(30);
        return;
    };
    
    return this;
}
