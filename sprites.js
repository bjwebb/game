var gamejs = require('gamejs');

var You = exports.You = function(scene) {
    You.superConstructor.apply(this, arguments);
     
    this.scene = scene;
    this.image = gamejs.image.load("images/k6.png");
    this.imageMaster = this.image;
    this.rect = this.image.getRect();
    this.x = 40;
    this.y = 40;
    this.speed = 0;
    this.maxspeed = 200;
    this.heading = 0;
    this.rotateDir = 0;
    return this;
}

gamejs.utils.objects.extend(You, gamejs.sprite.Sprite);

You.prototype.update = function(msDuration) {
    // move
    var theta = this.heading / 180 * Math.PI;
    this.x += Math.cos(theta) * this.speed * (msDuration/1000);
    this.y += Math.sin(theta) * this.speed * (msDuration/1000);
    this.rect.center = [this.x, this.y];

    if (this.x < 0) {
       this.x = this.scene.screen.getRect().right - 20;
    } else if (this.x > this.scene.screen.getRect().right) {
       this.x = 10;
    }
    if (this.y < 0) {
       this.y = this.scene.screen.getRect().bottom - 20;
    } else if (this.y > this.scene.screen.getRect().bottom) {
       this.y = 10;
    }

    return;
}



var Tortoise = exports.Tortoise = function(scene) {
    Tortoise.superConstructor.apply(this,  arguments);
     
    this.scene = scene;
    this.image = gamejs.image.load("images/tortoise.png");
    this.imageMaster = this.image;
    this.x = -120;
    this.y = 400;
    this.speed = 30;
    this.rect = this.image.getRect();
    return this;
}

gamejs.utils.objects.extend(Tortoise, gamejs.scene.MovingSprite);
